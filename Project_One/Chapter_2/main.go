package main

import (
	"chapter_2/operations"
	"fmt"
)

func main() {
	types := operations.LoadTypes()
	fmt.Println(types)
	cats := operations.LoadCats()
	for _, cat := range cats {
		fmt.Println("Название:", cat.Title)
		fmt.Println("Описание:", cat.Description)
		fmt.Println("Вид:", cat.CatType)
		fmt.Println("----------------------------------")
	}

}
