package operations

import (
	"bufio"
	"chapter_2/models"
	"errors"
	"fmt"
	"os"
	"strings"
)

/*
Функция возвращает массив типов кошачьих из файла
*/
func LoadTypes() []models.CatType {
	var types []models.CatType
	fileName := "types.txt"
	pwd, _ := os.Getwd()
	file, err := os.Open(pwd + "\\data\\" + fileName)
	if err != nil {
		fmt.Println("Ошибка при открытии файла")
	}
	id := 1
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		title := scanner.Text()
		scanner.Scan()
		description := scanner.Text()
		//Пропускаем пустую строку
		scanner.Scan()
		types = append(types, models.CatType{
			Id:          id,
			Title:       title,
			Description: description,
		})
		id++
	}
	err = file.Close()
	if err != nil {
		fmt.Println("Файл не закрыт")
	}
	if scanner.Err() != nil {
		fmt.Println("Ошибка при считывании файла")
	}
	return types
}

/*
Функция ищет в массиве типов котов структуру по названию типаа.
Функция возвращает структуру и nil вместо ошибки.
Если функция не находит структуру, то вернёт пустую структуру и ошибку.
*/
func findType(types []models.CatType, title string) (models.CatType, error) {
	//Делаем сплит строки title по : (см данные)
	splitted := strings.Split(title, ":")
	for _, catType := range types {
		if catType.Title == strings.TrimSpace(splitted[1]) {
			return catType, nil
		}
	}
	//Если не нашли то нужно вывести ошибку и пустой тип
	err := errors.New("Не найден тип кошачьих:" + splitted[1]) //ошибка
	result := models.CatType{}                                 //пустой тип
	return result, err
}

/*
Функция возвращает массив видов котов из файла
*/
func LoadCats() []models.Cat {
	//получаем список видов кошачьих
	types := LoadTypes()
	//Открываем файл с описанием видов котов
	fileName := "cats.txt"
	var cats []models.Cat
	pwd, _ := os.Getwd()
	file, err := os.Open(pwd + "\\data\\" + fileName)
	if err != nil {
		fmt.Println("Ошибка при открытии файла")
	}
	id := 1
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		catTitle := scanner.Text()
		scanner.Scan()
		catDescription := scanner.Text()
		scanner.Scan()
		catTypeString := scanner.Text()
		//пытаемся найти тип в массиве
		catRealType, err := findType(types, catTypeString)
		//если типа нет то выбрасываем ошибку
		if err != nil {
			panic(err)
		}
		//Пропускаем пустую строку
		scanner.Scan()
		cats = append(cats, models.Cat{
			Id:          id,
			Title:       catTitle,
			Description: catDescription,
			CatType:     catRealType,
		})
		id++
	}
	err = file.Close()
	if err != nil {

	}
	if scanner.Err() != nil {
		fmt.Println("Ошибка при считывании файла")
	}
	return cats
}
