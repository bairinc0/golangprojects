package operations

import (
	"bufio"
	"chapter_2/models"
	"errors"
	"fmt"
	"os"
	"strings"
)

/*
Функция возвращает массив типов кошачьих из файла
*/
func LoadTypes() []models.CatType {
	var types []models.CatType
	fileName := "types.txt"
	pwd, _ := os.Getwd()
	file, err := os.Open()	
	if err != nil {
		fmt.Println("Ошибка при открытии файла")
	}
	id := 1
	scanner := ???
	for scanner.Scan() {
		title := ???
		scanner.Scan()
		description := ???
		//Пропускаем пустую строку
		scanner.Scan()
		types = append(types, models.CatType{
			Id:          id,
			Title:       title,
			Description: description,
		})
		id++
	}
	err = file.Close()
	if err != nil {
		fmt.Println("Файл не закрыт")
	}
	if scanner.Err() != nil {
		fmt.Println("Ошибка при считывании файла")
	}
	return types
}

/*
Функция ищет в массиве типов котов структуру по названию типа.
Функция возвращает структуру и nil вместо ошибки.
Если функция не находит структуру, то вернёт пустую структуру и ошибку.
*/
func findType(types []models.CatType, title string) (models.CatType, error) {
	//Делаем сплит строки title по : (см данные)
	splitted := strings.Split(title, ":")
	for _, catType := range types {
		if catType.Title == strings.TrimSpace(splitted[1]) {
			return catType, nil
		}
	}
	//Если не нашли то нужно вывести ошибку и пустой тип
	err := errors.New("Не найден тип кошачьих:" + splitted[1]) //ошибка
	result := models.CatType{}                                 //пустой тип
	return result, err
}

/*
Функция возвращает массив типов котов из файла
*/
func LoadCats() []models.Cat {
	//получаем список типов кошачьих
	types := LoadTypes()
	//Открываем файл с описанием видов котов
	???	
	for ??? {
		???
		catTypeString := scanner.Text()
		//пытаемся найти тип в массиве
		catRealType, err := findType(types, catTypeString)
		//если типа нет то выбрасываем ошибку
		if err != nil {
			panic(err)
		}
		???
		cats = append(cats, models.Cat{
			Id:          ???,
			Title:       ???,
			Description: ???,
			CatType:     catRealType,
		})
		id++
	}
	err = file.Close()
	if err != nil {

	}
	if scanner.Err() != nil {
		fmt.Println("Ошибка при считывании файла")
	}
	return cats
}
