package models

type StringCat struct {
	Id          int
	Title       string
	Description string
	CatType     string
}
