package models

type CatType struct {
	Id          int
	Title       string
	Description string
}
