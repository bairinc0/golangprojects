package main

import (
	"bufio"
	"chapter_2/models"
	"fmt"
	"os"
)

func readCats() []models.StringCat {
	fileName := "cats.txt"
	var cats []models.StringCat
	pwd, _ := os.Getwd()
	file, err := os.Open(pwd + "\\..\\" + "\\data\\" + fileName)
	fmt.Println(pwd)
	if err != nil {
		fmt.Println("Ошибка при открытии файла")
	}
	id := 1
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		catTitle := scanner.Text()
		scanner.Scan()
		catDescription := scanner.Text()
		scanner.Scan()
		catType := scanner.Text()
		//Пропускаем пустую строку
		scanner.Scan()
		cats = append(cats, models.StringCat{
			Id:          id,
			Title:       catTitle,
			Description: catDescription,
			CatType:     catType,
		})
		id++
	}
	err = file.Close()
	if err != nil {

	}
	if scanner.Err() != nil {
		fmt.Println("Ошибка при считывании файла")
	}
	return cats
}

func main() {
	cats := readCats()
	for _, cat := range cats {
		fmt.Println("Название:", cat.Title)
		fmt.Println("Описание:", cat.Description)
		fmt.Println("Вид:", cat.CatType)
		fmt.Println("----------------------------------")
	}
}
