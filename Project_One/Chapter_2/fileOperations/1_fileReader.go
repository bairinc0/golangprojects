// Запускаем через go run fileReader.go!
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	fileName := "testFile.txt"
	var lines []string
	pwd, _ := os.Getwd()
	file, err := os.Open(pwd + "\\..\\" + "\\data\\" + fileName)
	fmt.Println(pwd)
	if err != nil {
		fmt.Println("Ошибка при открытии файла")
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	err = file.Close()
	if err != nil {

	}
	if scanner.Err() != nil {
		fmt.Println("Ошибка при считывании файла")
	}
	fmt.Println(lines)
}
