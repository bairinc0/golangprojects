package api

import (
	"chapter_4_c/db"
	"chapter_4_c/models"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

// теперь мы получаем ссылку на подключение внутри пакета
func getCatFromDB(writer http.ResponseWriter, request *http.Request) {
	id_param := request.URL.Query().Get("cat_id")
	id, _ := strconv.Atoi(id_param)
	cat := models.Cat{}
	cat.Find(id)
	writer.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(writer).Encode(cat)
	if err != nil {
		log.Fatal(err)
	}
}
func getCatTypeFromDB(writer http.ResponseWriter, request *http.Request) {
	id_param := request.URL.Query().Get("cat_type_id")
	id, _ := strconv.Atoi(id_param)
	catType := models.CatType{}
	catType.Find(id)
	writer.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(writer).Encode(catType)
	if err != nil {
		log.Fatal(err)
	}
}
func getObj(writer http.ResponseWriter, request *http.Request) {
	id_param := request.URL.Query().Get("id")
	id, _ := strconv.Atoi(id_param)
	objectType := request.URL.Query().Get("objectType")
	object, err := models.Factory(objectType, id)
	if err != nil {
		log.Fatal(err)
	}
	writer.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(writer).Encode(object)
	if err != nil {
		log.Fatal(err)
	}
}
func Start() {
	db.Connect("chapter_3_seed", "postgres", "1")
	defer db.Close()
	http.HandleFunc("/getCatFromDB/", getCatFromDB)
	http.HandleFunc("/getCatTypeFromDB/", getCatTypeFromDB)
	http.HandleFunc("/getObj/", getObj)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
