package models

import "errors"

// Добавим интерфейс для типизации
type DBObject interface {
}

func Factory(name string, id int) (DBObject, error) {
	if name == "Cat" {
		cat := Cat{}
		cat.Find(id)
		return cat, nil
	}
	if name == "CatType" {
		catType := CatType{}
		catType.Find(id)
		return catType, nil
	}
	return nil, errors.New("Неправильный тип")
}
