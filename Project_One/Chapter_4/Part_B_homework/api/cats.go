package api

import (
	"chapter_4_b_homework/db"
	"chapter_4_b_homework/models"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

func getCat(writer http.ResponseWriter, request *http.Request) {
	id_param := request.URL.Query().Get("cat_id")
	id, _ := strconv.Atoi(id_param)
	catType := &models.CatType{Id: 1, Title: "Домашние кошки", Description: "Описание домашних кошек"}
	cat := &models.Cat{Id: id, Title: "Сиамская кошка", Description: "Описание сиамской", CatType: *catType}
	writer.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(writer).Encode(cat)
	if err != nil {
		log.Fatal(err)
	}
}
func getCatFromDB(writer http.ResponseWriter, request *http.Request) {
	id_param := request.URL.Query().Get("cat_id")
	id, _ := strconv.Atoi(id_param)
	//подключаемся к БД
	dbConnect := db.Connect("chapter_3_seed", "postgres", "1")
	defer dbConnect.Close()
	cat := models.Cat{}
	cat.Find(dbConnect, id)
	writer.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(writer).Encode(cat)
	if err != nil {
		log.Fatal(err)
	}
}
func getCatTypeFromDB(writer http.ResponseWriter, request *http.Request) {
	id_param := request.URL.Query().Get("cat_type_id")
	id, _ := strconv.Atoi(id_param)
	//подключаемся к БД
	dbConnect := db.Connect("chapter_3_seed", "postgres", "1")
	defer dbConnect.Close()
	catType := models.CatType{}
	catType.Find(dbConnect, id)
	writer.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(writer).Encode(catType)
	if err != nil {
		log.Fatal(err)
	}
}
func Start() {
	http.HandleFunc("/getCat/", getCat)
	http.HandleFunc("/getCatFromDB/", getCatFromDB)
	http.HandleFunc("/getCatTypeFromDB/", getCatTypeFromDB)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
