package models

import "errors"

// Добавим интерфейс для типизации
type DBObject interface {
}

//Функция возвращает ссылку на объект по названию модели и id
func Factory(name string, id int) (DBObject, error) {
	if name == "Cat" {
		cat := Cat{}
		cat.Find(id)
		return cat, nil
	}
	if name == "CatType" {
		catType := CatType{}
		catType.Find(id)
		return catType, nil
	}
	return nil, errors.New("Неправильный тип")
}

//Часть D - Добавляем функцию для получения списка по названию
func FactoryList(name string) ([]DBObject, error) {
	if name == "CatType" {
		list := AllCatTypes()
		result := make([]DBObject, len(list))
		for i, v := range list {
			result[i] = v
		}
		return result, nil
	}
	if name == "Cat" {
		list := AllCats()
		result := make([]DBObject, len(list))
		for i, v := range list {
			result[i] = v
		}
		return result, nil
	}
	return nil, errors.New("Неправильный тип")
}
