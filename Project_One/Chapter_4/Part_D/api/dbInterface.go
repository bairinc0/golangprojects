package api

import (
	"chapter_4_d/models"
	"encoding/json"
	"errors"
)

func saveObjToDB(data *json.Decoder, objectType string) error {
	if objectType == "CatType" {
		obj := models.CatType{}
		var err error
		err = data.Decode(&obj)
		if err == nil {
			err = obj.Save()
		}
		return err
	}
	return errors.New("Неверный тип данных")
}
func deleteObjFromDB(objectType string, id int) error {
	if objectType == "CatType" {
		obj := models.CatType{}
		var err error
		err = obj.Find(id)
		if err == nil {
			err = obj.Delete()
		}
		return err
	}
	return errors.New("Неверный тип данных")
}
