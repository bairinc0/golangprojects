package api

import (
	"chapter_4_d/db"
	"chapter_4_d/models"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

func getObj(writer http.ResponseWriter, request *http.Request) {
	id_param := request.URL.Query().Get("id")
	id, _ := strconv.Atoi(id_param)
	objectType := request.URL.Query().Get("objectType")
	object, err := models.Factory(objectType, id)
	if err != nil {
		log.Fatal(err)
	}
	writer.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(writer).Encode(object)
	if err != nil {
		log.Fatal(err)
	}
}

// Часть D - функция возвращает списки объектов
func getObjList(writer http.ResponseWriter, request *http.Request) {
	objectType := request.URL.Query().Get("objectType")
	object, err := models.FactoryList(objectType)
	if err != nil {
		log.Fatal(err)
	}
	writer.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(writer).Encode(object)
	if err != nil {
		log.Fatal(err)
	}
}

// Часть D - функция для сохранения нового объекта в базе, сохранение сразу работает как и UPDATE
func saveObj(writer http.ResponseWriter, request *http.Request) {
	objectType := request.URL.Query().Get("objectType")
	err := saveObjToDB(json.NewDecoder(request.Body), objectType)
	msgStr := ""
	if err != nil {
		log.Fatal(err)
	} else {
		msgStr = "save successful"
	}
	message := []byte(msgStr)
	_, err = writer.Write(message)
	if err != nil {
		log.Fatal(err)
	}
}

// Часть D - функция для удаления объекта из базы
func deleteObj(writer http.ResponseWriter, request *http.Request) {
	id_param := request.URL.Query().Get("id")
	id, _ := strconv.Atoi(id_param)
	objectType := request.URL.Query().Get("objectType")
	err := deleteObjFromDB(objectType, id)
	msgStr := ""
	if err != nil {
		log.Fatal(err)
	} else {
		msgStr = "delete successful"
	}
	message := []byte(msgStr)
	_, err = writer.Write(message)
	if err != nil {
		log.Fatal(err)
	}
}

func Start() {
	db.Connect("chapter_3_seed", "postgres", "1")
	defer db.Close()
	http.HandleFunc("/getObj/", getObj)
	http.HandleFunc("/getObjList/", getObjList)
	http.HandleFunc("/saveObj/", saveObj)
	http.HandleFunc("/deleteObj/", deleteObj)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
