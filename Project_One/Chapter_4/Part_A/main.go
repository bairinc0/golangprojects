package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

type CatType struct {
	Id          int
	Title       string
	Description string
}
type Cat struct {
	Id          int
	Title       string
	Description string
	CatType     CatType
}

func getCat(writer http.ResponseWriter, request *http.Request) {
	//получим параметр!
	id_param := request.URL.Query().Get("cat_id")
	id, _ := strconv.Atoi(id_param)
	catType := &CatType{Id: 1, Title: "Домашние кошки", Description: "Описание домашних кошек"}
	cat := &Cat{Id: id, Title: "Сиамская кошка", Description: "Описание сиамских кошек", CatType: *catType}
	writer.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(writer).Encode(cat)
	if err != nil {
		log.Fatal(err)
	}
}
func postCat(writer http.ResponseWriter, request *http.Request) {
	//writer.Header().Set("Content-Type", "application/json")
	var data Cat
	err := json.NewDecoder(request.Body).Decode(&data)
	msgStr := ""
	if err != nil {
		log.Fatal(err)
	} else {
		msgStr = "request successful:" + data.Title + "(" + data.CatType.Title + ")"
	}
	message := []byte(msgStr)
	_, err = writer.Write(message)
	if err != nil {
		log.Fatal(err)
	}
}
func main() {
	http.HandleFunc("/getCat/", getCat)
	http.HandleFunc("/postCat", postCat)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
