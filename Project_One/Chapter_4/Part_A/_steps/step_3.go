package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type person struct {
	Name   string
	Family string
	Age    int
}

// примем POST Запрос
func postRequest(writer http.ResponseWriter, request *http.Request) {
	//writer.Header().Set("Content-Type", "application/json")
	var data person
	err := json.NewDecoder(request.Body).Decode(&data)
	msgStr := ""
	if err != nil {
		log.Fatal(err)
	} else {
		msgStr = "request successful:" + data.Family
	}
	message := []byte(msgStr)
	_, err = writer.Write(message)
	if err != nil {
		log.Fatal(err)
	}
}
func main() {
	http.HandleFunc("/postExample", postRequest)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
