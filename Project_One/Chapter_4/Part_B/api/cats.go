package api

import (
	"chapter_4_b/db"
	"chapter_4_b/models"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

func getCat(writer http.ResponseWriter, request *http.Request) {
	//получим параметр!
	id_param := request.URL.Query().Get("cat_id")
	id, _ := strconv.Atoi(id_param)
	catType := &models.CatType{Id: 1, Title: "Домашние кошки", Description: "Описание домашних кошек"}
	cat := &models.Cat{Id: id, Title: "Сиамская кошка", Description: "Описание сиамских кошек", CatType: *catType}
	writer.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(writer).Encode(cat)
	if err != nil {
		log.Fatal(err)
	}
}
func getCatFromDB(writer http.ResponseWriter, request *http.Request) {
	//получим параметр!
	id_param := request.URL.Query().Get("cat_id")
	id, _ := strconv.Atoi(id_param)
	//цепляемся к базе
	dbConnect := db.Connect("chapter_3_seed", "postgres", "1")
	defer dbConnect.Close()
	cat := models.Cat{}
	//делаем запрос
	cat.Find(dbConnect, id)
	writer.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(writer).Encode(cat)
	if err != nil {
		log.Fatal(err)
	}
}
func Start() {
	http.HandleFunc("/getCat/", getCat)
	http.HandleFunc("/getCatFromDB/", getCatFromDB)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
