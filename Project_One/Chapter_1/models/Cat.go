package models

type Cat struct {
	Id          int
	Title       string
	Description string
	CatType     int
}
