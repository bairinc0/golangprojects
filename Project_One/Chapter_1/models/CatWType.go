package models

type CatWType struct {
	Id          int
	Title       string
	Description string
	CatType     CatType
}
