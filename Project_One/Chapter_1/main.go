package main

import (
	"chapter_1/models"
	"fmt"
)

func main() {
	cat1 := models.Cat{1, "Сиамская кошка", "Сиамские кошки - вид кошек", 1}
	fmt.Println(cat1)
	cat2 := models.Cat{
		Id:          2,
		Title:       "Сиамская кошка",
		Description: "Сиамские кошки - вид кошек",
		CatType:     1,
	}
	fmt.Println(cat2)
	cat3 := models.CatWType{
		Id:          3,
		Title:       "Тигр",
		Description: "Тигры",
		CatType:     models.CatType{3, "тигры", "тигры"},
	}
	fmt.Println(cat3)
}
