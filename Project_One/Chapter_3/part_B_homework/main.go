package main

import (
	"chapter_3_b_homework/db"
	"fmt"
)

func main() {
	dbConnect := db.Connect("chapter_3", "postgres", "1")
	defer dbConnect.Close()
	catType := db.FindCatTypeById(dbConnect, 1)
	fmt.Println(catType)
	cats := db.AllCats(dbConnect)
	//fmt.Println(cats)
	for _, cat := range cats {
		fmt.Println(cat.CatInfo())
	}
}
