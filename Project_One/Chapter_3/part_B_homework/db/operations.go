package db

import (
	"chapter_3_b_homework/models"
	"database/sql"

	_ "github.com/lib/pq"
)

func Connect(dbname string, user string, password string) *sql.DB {
	connStr := "user=" + user + " password=" + password + " dbname=" + dbname + " sslmode=disable"
	dbConnect, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	return dbConnect
}

func AllCatTypes(dbConnect *sql.DB) []models.CatType {
	var result []models.CatType
	rows, err := dbConnect.Query("SELECT id,title,description FROM cat_types")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		p := models.CatType{}
		err := rows.Scan(&p.Id, &p.Title, &p.Description)
		if err != nil {
			panic(err)
		}
		result = append(result, p)
	}
	return result
}
func FindCatTypeById(dbConnect *sql.DB, cat_type_id int) models.CatType {
	var result models.CatType
	err := dbConnect.QueryRow("SELECT id,title,description FROM cat_types WHERE id = $1", cat_type_id).Scan(&result.Id, &result.Title, &result.Description)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
	}
	return result
}
func AllCats(dbConnect *sql.DB) []models.Cat {
	var result []models.Cat
	rows, err := dbConnect.Query("SELECT id,title,description,cat_type_id FROM cats")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		cat := models.Cat{}
		var cat_type_id int
		err := rows.Scan(&cat.Id, &cat.Title, &cat.Description, &cat_type_id)
		if err != nil {
			panic(err)
		}
		catType := FindCatTypeById(dbConnect, cat_type_id)
		cat.CatType = catType
		result = append(result, cat)
	}
	return result
}
