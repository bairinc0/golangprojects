package models

type CatType struct {
	Id          int
	Title       string
	Description string
}

type Cat struct {
	Id          int
	Title       string
	Description string
	CatType     CatType
}

func (cat Cat) CatInfo() string {
	result := cat.Title + " (" + cat.CatType.Title + ") \n"
	result += cat.Description
	result += "\n--------------------------------------"
	return result
}
