package main

import (
	"chapter_3_c/db"
	"chapter_3_c/models"
	"fmt"
)

func main() {
	dbConnect := db.Connect("chapter_3", "postgres", "1")
	defer dbConnect.Close()
	operation := false
	if operation {
		myCat := models.Cat{Title: "Сиамская кошка", Description: "Описание сиамской", CatType: models.CatType{Id: 2, Title: "Какой то тип", Description: "Какой то описание"}}
		id, err := myCat.Save(dbConnect)
		if err != nil {
			panic(err)
		}
		fmt.Println("new cat id:", id)
	}
	operation = false
	if operation {
		newType := models.CatType{Title: "Новый тип кошек", Description: "Новое описание"}
		err := newType.Save(dbConnect)
		if err != nil {
			panic(err)
		}
		fmt.Println("new cat type id:", newType.Id)
		newType.Title = "Изменяем название типа"
		newType.Save(dbConnect)
	}
	operation = false
	if operation {
		cats := db.AllCats(dbConnect)
		//fmt.Println(cats)
		for _, cat := range cats {
			fmt.Println(cat.CatInfo())
		}
	}
	operation = true
	if operation {
		typeById := models.CatType{}
		typeById.Find(dbConnect, 1)
		fmt.Println(typeById)
		typeByName := models.CatType{}
		typeByName.FindByTitle(dbConnect, "Домашняя кошка")
		fmt.Println(typeByName)
	}
}
