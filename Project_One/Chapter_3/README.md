# Подготовка
## Установка PostgreSQL
https://www.postgresqltutorial.com/postgresql-getting-started/install-postgresql/#:~:text=PostgreSQL%20was%20developed%20for%20UNIX,installation%20process%20easier%20and%20faster.

## Скачать PostgreSQL
https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
## Работа с PostgreSQL
https://metanit.com/go/tutorial/10.3.php

# hints
Не забываем:
в каждом модуле: go get github.com/lib/pq
в первый раз - go mod tidy (!!)

# Содержание
В главе 3 мы:
1. создаём базу данных в СУБД Postgre SQL
2. Реализуем запросы
3. Разрабатываем пакеты для работы с СУБД

## Часть А (part_A)
Установка и настройка СУБД
Запросы из примера
## Часть Б (part_B) + домашняя работа (part_B_homework)
Создаём таблицы для типов котов и котов 
Cоздаём отдельный пакет для работы с базой
## Часть С (part_C) 
Создаём "ORM" для работы с базой
## Часть D 
Создаём сидер - в "пустой базе" мы разворачиваем таблицы с данными