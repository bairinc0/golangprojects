package main

import (
	"chapter_3_d/db"
	"chapter_3_d/seed"
)

func main() {
	dbConnect := db.Connect("chapter_3_seed", "postgres", "1")
	defer dbConnect.Close()
	seed.Init(dbConnect)
}
