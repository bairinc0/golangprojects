package seed

import (
	"chapter_3_d/models"
	"database/sql"
)

func seedCatTypes(dbConnect *sql.DB) {
	catTypes := LoadTypes()
	for _, catType := range catTypes {
		err := catType.Save(dbConnect)
		if err != nil {
			panic(err)
		}
	}
}
func seedCats(dbConnect *sql.DB) {
	cats := LoadCats()
	for _, cat := range cats {
		//подгрузим тип кота
		realCatType := models.CatType{}
		realCatType.FindByTitle(dbConnect, cat.CatType.Title)
		cat.CatType = realCatType
		err := cat.Save(dbConnect)
		if err != nil {
			panic(err)
		}
	}
}
