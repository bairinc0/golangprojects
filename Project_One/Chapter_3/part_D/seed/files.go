package seed

import (
	"bufio"
	"chapter_3_d/models"
	"fmt"
	"os"
	"strings"
)

/*
Функция возвращает массив типов кошачьих из файла
*/
func LoadTypes() []models.CatType {
	var types []models.CatType
	fileName := "types.txt"
	pwd, _ := os.Getwd()
	file, err := os.Open(pwd + "\\data\\" + fileName)
	if err != nil {
		fmt.Println("Ошибка при открытии файла")
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		title := scanner.Text()
		scanner.Scan()
		description := scanner.Text()
		//Пропускаем пустую строку
		scanner.Scan()
		types = append(types, models.CatType{
			Title:       title,
			Description: description,
		})
	}
	defer file.Close()
	if scanner.Err() != nil {
		fmt.Println("Ошибка при считывании файла")
	}
	return types
}

func LoadCats() []models.Cat {
	//Открываем файл с описанием видов котов
	fileName := "cats.txt"
	var cats []models.Cat
	pwd, _ := os.Getwd()
	file, err := os.Open(pwd + "\\data\\" + fileName)
	if err != nil {
		fmt.Println("Ошибка при открытии файла")
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		catTitle := scanner.Text()
		scanner.Scan()
		catDescription := scanner.Text()
		scanner.Scan()
		catTypeString := strings.Split(scanner.Text(), ":")
		//ищем в базе
		catType := models.CatType{Title: strings.TrimSpace(catTypeString[1])}
		//Пропускаем пустую строку
		scanner.Scan()
		cats = append(cats, models.Cat{
			Title:       catTitle,
			Description: catDescription,
			CatType:     catType,
		})
	}
	defer file.Close()
	if scanner.Err() != nil {
		fmt.Println("Ошибка при считывании файла")
	}
	return cats
}
