package seed

import (
	"database/sql"

	_ "github.com/lib/pq"
)

type dbtable struct {
	tableName string
	tableRows []string
}

func createTable(dbConnect *sql.DB, table dbtable) error {
	rows := ""
	for ind, row := range table.tableRows {
		rows += row
		if ind < len(table.tableRows)-1 {
			rows += ","
		}
	}
	query := "CREATE TABLE IF NOT EXISTS " + table.tableName + "(" + rows + ")"
	_, err := dbConnect.Exec(query)
	return err
}
func createTables(dbConnect *sql.DB) {
	catTypes := dbtable{
		tableName: "cat_types",
		tableRows: []string{"\"id\" integer PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY",
			"\"title\" text NOT NULL",
			"\"description\" text NOT NULL",
		},
	}
	cats := dbtable{
		tableName: "cats",
		tableRows: []string{"\"id\" integer PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY",
			"\"title\" text NOT NULL",
			"\"description\" text NOT NULL",
			"\"cat_type_id\" integer",
		},
	}
	err := createTable(dbConnect, catTypes)
	if err != nil {
		panic(err)
	}
	err = createTable(dbConnect, cats)
	if err != nil {
		panic(err)
	}
}
func Init(dbConnect *sql.DB) {
	createTables(dbConnect)
	seedCatTypes(dbConnect)
	seedCats(dbConnect)
}
