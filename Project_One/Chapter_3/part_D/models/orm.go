package models

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"
)

func (cat *Cat) Save(dbConnect *sql.DB) error {
	var err error
	if cat.Id == 0 {
		var id int
		err = dbConnect.QueryRow("INSERT INTO cats (title,description,cat_type_id) VALUES ($1,$2,$3) returning id",
			cat.Title, cat.Description, cat.CatType.Id).Scan(&id)
		if err == nil {
			cat.Id = id
		}
	} else {
		_, err = dbConnect.Exec("UPDATE cats SET title=$1, description=$2,cat_type_id=$3 WHERE id=$4",
			cat.Title, cat.Description, cat.CatType.Id, cat.Id)
	}
	return err
}
func (cat *Cat) Find(dbConnect *sql.DB, cat_id int) error {
	var cat_type_id int
	err := dbConnect.QueryRow("SELECT id,title,description,cat_type_id FROM cats WHERE id=$1", cat_id).Scan(&cat.Id, &cat.Title, &cat.Description, &cat_type_id)
	catType := CatType{}
	err = catType.Find(dbConnect, cat_type_id)
	if err != nil {
		panic(err)
	}
	cat.CatType = catType
	return err
}
func (cat *Cat) Delete(dbConnect *sql.DB) error {
	var err error
	if cat.Id != 0 {
		var result sql.Result
		result, err = dbConnect.Exec("DELETE FROM cats WHERE id=$1", cat.Id)
		if err == nil {
			affRows, _ := result.RowsAffected()
			if affRows == 0 {
				err = errors.New("Не было удалено строк!")
			} else {
				cat.Id = 0
			}
		}
	}
	return err
}
func (catType *CatType) Save(dbConnect *sql.DB) error {
	var err error
	if catType.Id == 0 {
		var id int
		err = dbConnect.QueryRow("INSERT INTO cat_types (title,description) VALUES ($1,$2) returning id",
			catType.Title, catType.Description).Scan(&id)
		if err == nil {
			catType.Id = id
		}
	} else {
		_, err = dbConnect.Exec("UPDATE cat_types SET title=$1, description=$2 WHERE id=$3",
			catType.Title, catType.Description, catType.Id)
	}
	return err
}
func (catType *CatType) Find(dbConnect *sql.DB, cat_type_id int) error {
	err := dbConnect.QueryRow("SELECT id,title,description FROM cat_types WHERE id=$1", cat_type_id).Scan(&catType.Id, &catType.Title, &catType.Description)
	return err
}
func (catType *CatType) Delete(dbConnect *sql.DB) error {
	var err error
	if catType.Id != 0 {
		var result sql.Result
		result, err = dbConnect.Exec("DELETE FROM cat_types WHERE id=$1", catType.Id)
		if err == nil {
			affRows, _ := result.RowsAffected()
			if affRows == 0 {
				err = errors.New("Не было удалено строк!")
			} else {
				catType.Id = 0
			}
		}
	}
	return err
}
func (catType *CatType) FindByTitle(dbConnect *sql.DB, type_title string) error {
	err := dbConnect.QueryRow("SELECT id,title,description FROM cat_types WHERE title=$1 LIMIT 1", type_title).Scan(&catType.Id, &catType.Title, &catType.Description)
	return err
}
