package main

import (
	"chapter_3_b/db"
	"fmt"
)

func main() {
	dbConnect := db.Connect("chapter_3", "postgres", "1")
	defer dbConnect.Close()
	catTypes := db.AllCatTypes(dbConnect)
	for _, p := range catTypes {
		fmt.Println(p.Id, p.Title, p.Description)
	}
}
