CREATE TABLE IF NOT EXISTS public.cat_types
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    description text COLLATE pg_catalog."default",
    title character varying COLLATE pg_catalog."default",
    CONSTRAINT cat_types_pkey PRIMARY KEY (id)
)
