package models

type CatType struct {
	Id          int
	Title       string
	Description string
}

type Cat struct {
	Id          int
	Title       string
	Description string
	CatType     CatType
}
