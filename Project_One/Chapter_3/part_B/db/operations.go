package db

import (
	"chapter_3_b/models"
	"database/sql"

	_ "github.com/lib/pq"
)

func Connect(dbname string, user string, password string) *sql.DB {
	connStr := "user=" + user + " password=" + password + " dbname=" + dbname + " sslmode=disable"
	dbConnect, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	return dbConnect
}

func AllCatTypes(dbConnect *sql.DB) []models.CatType {
	var result []models.CatType
	rows, err := dbConnect.Query("SELECT id,title,description FROM cat_types")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		p := models.CatType{}
		err := rows.Scan(&p.Id, &p.Title, &p.Description)
		if err != nil {
			panic(err)
		}
		result = append(result, p)
	}
	return result
}
