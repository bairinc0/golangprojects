package db

import (
	"chapter_3_c_homework/models"
	"database/sql"

	_ "github.com/lib/pq"
)

func Connect(dbname string, user string, password string) *sql.DB {
	connStr := "user=" + user + " password=" + password + " dbname=" + dbname + " sslmode=disable"
	dbConnect, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	return dbConnect
}

func AllCatTypes(dbConnect *sql.DB) []models.CatType {
	var result []models.CatType
	rows, err := dbConnect.Query("SELECT id,title,description FROM cat_types")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		catType := models.CatType{}
		err := rows.Scan(&catType.Id, &catType.Title, &catType.Description)
		if err != nil {
			panic(err)
		}
		result = append(result, catType)
	}
	return result
}

func AllCats(dbConnect *sql.DB) []models.Cat {
	var result []models.Cat
	rows, err := dbConnect.Query("SELECT id,title,description,cat_type_id FROM cats")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		cat := models.Cat{}
		var cat_type_id int
		err := rows.Scan(&cat.Id, &cat.Title, &cat.Description, &cat_type_id)
		if err != nil {
			panic(err)
		}
		err = cat.CatType.Find(dbConnect, cat_type_id)
		if err != nil {
			panic(err)
		}
		result = append(result, cat)
	}
	return result
}
