package main

import (
	"chapter_3_c_homework/db"
	"chapter_3_c_homework/models"
	"fmt"
)

func main() {
	dbConnect := db.Connect("chapter_3", "postgres", "1")
	defer dbConnect.Close()
	operation := false
	if operation {
		myCat := models.Cat{Id: 7, Title: "Английская короткошёрстная 3",
			Description: "Описание Английской 3",
			CatType:     models.CatType{Id: 2},
		}
		err := myCat.Save(dbConnect)
		if err != nil {
			panic(err)
		}
		fmt.Println("New Cat id:", myCat.Id)
	}
	operation = false
	if operation {
		catById := models.Cat{}
		err := catById.Find(dbConnect, 2)
		if err != nil {
			panic(err)
		}
		fmt.Println(catById.CatInfo())
	}
	operation = true
	if operation {
		catById := models.Cat{}
		err := catById.Find(dbConnect, 7)
		if err != nil {
			panic(err)
		}
		catById.Delete(dbConnect)
	}
}
