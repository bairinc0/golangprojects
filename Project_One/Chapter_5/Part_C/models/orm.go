package models

import (
	"chapter_5_c/db"

	_ "github.com/lib/pq"
)

func FindUser(login string, password string) (User, error) {
	user := User{Id: 0}
	err := db.DbConnect.QueryRow("SELECT id,login,password FROM users WHERE login=$1 AND password=$2 LIMIT 1", login, password).Scan(&user.Id, &user.Login, &user.Password)
	return user, err
}
