package db

import (
	"database/sql"

	_ "github.com/lib/pq"
)

// общий коннект к базе
var (
	DbConnect *sql.DB
)

// оставляем только основные операции для работы с базой
func Connect(dbname string, user string, password string) {
	connStr := "user=" + user + " password=" + password + " dbname=" + dbname + " sslmode=disable"
	var err error
	DbConnect, err = sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
}
func Close() {
	DbConnect.Close()
}
