package main

//Пример двух страниц

import (
	"encoding/json"
	"log"
	"net/http"
)

type person struct {
	Name   string
	Family string
	Age    int
}

// Открытая страница
func exampleRequest(writer http.ResponseWriter, request *http.Request) {
	msg := "Привет мир!"
	//формируем заголовок
	writer.WriteHeader(http.StatusCreated)
	message := []byte(msg)
	_, err := writer.Write(message)
	if err != nil {
		log.Fatal(err)
	}
}

// как бы закрытая страница
func jsonRequest(writer http.ResponseWriter, request *http.Request) {
	user := &person{Name: "Иван", Family: "Иванов", Age: 25}
	writer.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(writer).Encode(user)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	http.HandleFunc("/example", exampleRequest)
	http.HandleFunc("/getExample", jsonRequest)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
