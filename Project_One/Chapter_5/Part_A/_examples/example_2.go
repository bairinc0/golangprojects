package main

//Пример двух страниц

import (
	"encoding/json"
	"log"
	"net/http"
)

type person struct {
	Name   string
	Family string
	Age    int
}

// структура для логинов
type user struct {
	Login    string
	Password string
}

// Открытая страница
func exampleRequest(writer http.ResponseWriter, request *http.Request) {
	msg := "Привет мир!"
	//формируем заголовок
	writer.WriteHeader(http.StatusCreated)
	message := []byte(msg)
	_, err := writer.Write(message)
	if err != nil {
		log.Fatal(err)
	}
}

// Закрытая страница
func jsonRequest(writer http.ResponseWriter, request *http.Request) {
	var account user
	err := json.NewDecoder(request.Body).Decode(&account)
	if err != nil {
		log.Fatal(err)
	}
	if haveAccess(account) {
		user := &person{Name: "Иван", Family: "Иванов", Age: 25}
		writer.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(writer).Encode(user)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		message := []byte("Вы не авторизованы!")
		_, err := writer.Write(message)
		if err != nil {
			log.Fatal(err)
		}
	}

}

// проверяем логин и пароль
func haveAccess(account user) bool {
	//эмулируем обращение в базу и проверку
	if account.Login == "Cow" && account.Password == "SecretPasswd0309" {
		return true
	}
	return false
}
func main() {
	http.HandleFunc("/example", exampleRequest)
	http.HandleFunc("/getExample", jsonRequest)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
