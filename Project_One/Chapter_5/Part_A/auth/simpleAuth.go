package auth

//Структура для учётных записей
type User struct {
	Id       int
	Login    string
	Password string
}

// статус авторизации
var (
	isLogged bool
)

// метод подключения
func (account User) LoginAccount() {
	//эмулируем обращение в базу и проверку
	if account.Login == "Cow" && account.Password == "SecretPasswd0309" {
		isLogged = true
	} else {
		isLogged = false
	}
}

//метод для проверки логина
func IsLogged() bool {
	return isLogged
}

//метод для разлогинивания
func Logout() {
	if isLogged {
		isLogged = false
	}
}
