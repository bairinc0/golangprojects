package auth

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

// Структура для учётных записей
type User struct {
	Id       int
	Login    string
	Password string
}

// метод подключения - теперь вернём токен
func (account User) LoginAccount() (string, error) {
	//эмулируем обращение в базу и проверку
	if account.Login == "Cow" && account.Password == "SecretPasswd0309" {
		return getToken("cow@bsu.ru")
	} else {
		return "wrong!", errors.New("Токен не был создан. Неверный логин/пароль")
	}
}

// проверка логина
func IsLogged(request *http.Request) bool {
	var token string
	err := json.NewDecoder(request.Body).Decode(&token)
	if err != nil {
		log.Fatal(err)
	}
	return checkToken(token)
}

// метод для разлогинивания
func Logout() {
	//резерв! - нужно удалять токен?
}

// -------------- Работаем с токенами
var jwtSecretKey = []byte("chapter_5_b")

// метод вернёт токен в случае верного логина и пароля
func getToken(email string) (string, error) {
	// Генерируем полезные данные, которые будут храниться в токене
	payload := jwt.MapClaims{
		"Email": email,                                   //почта
		"exp":   time.Now().Add(time.Minute * 10).Unix(), //время завершения
	}
	// Создаем новый JWT-токен и подписываем его по алгоритму HS256
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)
	t, err := token.SignedString(jwtSecretKey)
	return t, err
}

type MyClaims struct {
	Email string `json:"email"`
	jwt.RegisteredClaims
}

func checkToken(tokenString string) bool {
	token, err := jwt.ParseWithClaims(tokenString, &MyClaims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecretKey, nil
	}, jwt.WithLeeway(5*time.Second))
	if err != nil {
		return false
	}
	if _, ok := token.Claims.(*MyClaims); ok && token.Valid {
		return true
	} else {
		return false
	}
}
