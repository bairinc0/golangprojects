package main

//Пример двух страниц

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type person struct {
	Name   string
	Family string
	Age    int
}

// Открытая страница
func exampleRequest(writer http.ResponseWriter, request *http.Request) {
	msg := "Привет мир!"
	//формируем заголовок
	writer.WriteHeader(http.StatusCreated)
	message := []byte(msg)
	_, err := writer.Write(message)
	if err != nil {
		log.Fatal(err)
	}
}

// как бы закрытая страница
func jsonRequest(writer http.ResponseWriter, request *http.Request) {
	var token string
	err := json.NewDecoder(request.Body).Decode(&token)
	if err != nil {
		log.Fatal(err)
	}
	if checkToken(token) {
		user := &person{Name: "Иван", Family: "Иванов", Age: 25}
		writer.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(writer).Encode(user)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		err = json.NewEncoder(writer).Encode("вы не авторизованы")
		if err != nil {
			log.Fatal(err)
		}
	}

}

var jwtSecretKey = []byte("very-secret-key")

func getToken(writer http.ResponseWriter, request *http.Request) {
	// Генерируем полезные данные, которые будут храниться в токене
	payload := jwt.MapClaims{
		"Email": "cow@gmail.com",                    //почта
		"exp":   time.Now().Add(time.Minute).Unix(), //время завершения
	}
	// Создаем новый JWT-токен и подписываем его по алгоритму HS256
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)
	t, err := token.SignedString(jwtSecretKey)
	err = json.NewEncoder(writer).Encode(t)
	if err != nil {
		log.Fatal(err)
	}
}

type MyClaims struct {
	Email string `json:"email"`
	jwt.RegisteredClaims
}

func checkToken(tokenString string) bool {
	token, err := jwt.ParseWithClaims(tokenString, &MyClaims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecretKey, nil
	}, jwt.WithLeeway(5*time.Second))
	if err != nil {
		return false
	}
	if _, ok := token.Claims.(*MyClaims); ok && token.Valid {
		return true
	} else {
		return false
	}
}
func tokenInfo(writer http.ResponseWriter, request *http.Request) {
	var tokenString string
	err := json.NewDecoder(request.Body).Decode(&tokenString)
	if err != nil {
		log.Fatal(err)
	}

	token1, _ := jwt.ParseWithClaims(tokenString, &MyClaims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecretKey, nil
	}, jwt.WithLeeway(5*time.Second))
	err = json.NewEncoder(writer).Encode(token1)
	if err != nil {
		log.Fatal(err)
	}

}
func main() {
	http.HandleFunc("/example", exampleRequest)
	http.HandleFunc("/getExample", jsonRequest)
	http.HandleFunc("/getToken", getToken)
	http.HandleFunc("/tokenInfo", tokenInfo)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
