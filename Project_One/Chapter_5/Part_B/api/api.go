package api

import (
	"chapter_5_b/auth"
	"encoding/json"
	"log"
	"net/http"
)

type person struct {
	Name   string
	Family string
	Age    int
}

// Открытая страница
func exampleRequest(writer http.ResponseWriter, request *http.Request) {
	msg := "Привет мир!"
	//формируем заголовок
	writer.WriteHeader(http.StatusCreated)
	message := []byte(msg)
	_, err := writer.Write(message)
	if err != nil {
		log.Fatal(err)
	}
}

// страница авторизации
func login(writer http.ResponseWriter, request *http.Request) {
	var account auth.User
	err := json.NewDecoder(request.Body).Decode(&account)
	if err != nil {
		log.Fatal(err)
	}
	var msg string
	t, errT := account.LoginAccount()
	if errT == nil {
		msg = t
	} else {
		msg = "Wrong login data"
	}
	message := []byte(msg)
	_, err = writer.Write(message)
	if err != nil {
		log.Fatal(err)
	}
}

// Закрытая страница
func jsonRequest(writer http.ResponseWriter, request *http.Request) {
	if auth.IsLogged(request) {
		user := &person{Name: "Иван", Family: "Иванов", Age: 25}
		writer.Header().Set("Content-Type", "application/json")
		err := json.NewEncoder(writer).Encode(user)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		message := []byte("Вы не авторизованы!")
		_, err := writer.Write(message)
		if err != nil {
			log.Fatal(err)
		}
	}

}

func Start() {
	http.HandleFunc("/example", exampleRequest)
	http.HandleFunc("/getExample", jsonRequest)
	http.HandleFunc("/login", login)

	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
