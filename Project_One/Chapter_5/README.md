## Токены и авторизация 

## Подготовка (что можно почитать)

# Часть А. Пример "классической" авторизации
"Классическая" система авторизации

# Часть Б. Токены
https://github.com/golang-jwt/jwt
https://pkg.go.dev/github.com/golang-jwt/jwt/v5#ParseWithClaims
1. В папке с модулем: go get -u github.com/golang-jwt/jwt/v5
2. import "github.com/golang-jwt/jwt/v5"

Большой пример с токенами: https://ru.hexlet.io/courses/go-web-development/lessons/auth/theory_unit

# Часть С. Подсистема авторизации
В базе создаём (руками) специальную таблицу с логинами, меняем функцию для проверки логина и пароля
go get github.com/lib/pq